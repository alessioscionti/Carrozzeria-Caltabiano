<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('calendaries', function (Blueprint $table) {
            $table->id();
            $table->text('giorno')->nullable();
            $table->text('apertura')->nullable();
            $table->text('chiusura')->nullable();
            $table->boolean('visibile')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('calendaries');
    }
};
