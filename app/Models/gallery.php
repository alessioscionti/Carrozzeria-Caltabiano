<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class gallery extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_category',
        'path',
    ];

    public function category(){
        return $this->belongsTo(category::class,'id_category','id');
     }
}
