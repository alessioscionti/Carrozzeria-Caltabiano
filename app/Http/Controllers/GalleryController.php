<?php

namespace App\Http\Controllers;

use App\Models\gallery;
use App\Models\category;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Intervention\Image\Facades\Image as ResizeImage;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(gallery $gallery)
    {
        $galleries=gallery::with('category')->get()->groupBy('category.name');
        $category=category::all();
        
        return view('gallery.gallery',compact('galleries','category'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(category $categorie)
    {
        $categorie=category::all();
        return view('gallery.create',compact('categorie'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, gallery $gallery,category $categoria)
    {

        
        
        
        $save_pathThumb="images/categoria/thumbnails/".$request->categorie;
        $save_pathOriginal="images/categoria/".$request->categorie;
        $image = $request->file('immagine');
        $input['imagename'] = time().'.'.$image->extension();
        if (!file_exists($save_pathThumb)) {
            mkdir($save_pathThumb, 666, true);
        }
        $destinationPath = storage_path($save_pathThumb.'/thumbnail');
        //dd($destinationPath.'/'.$input['imagename']);
        $img = Image::make($image->path());
        $img->resize(800, 600, function ($constraint) {
            $constraint->aspectRatio();
        })->save("images/categoria/thumbnails/".$request->categorie.'/'.$input['imagename']);
   
        $destinationPath = storage_path($save_pathOriginal);
        $image->move($destinationPath, $input['imagename']);
        
        $gallery->id_category=$request->categorie;
        $gallery->path="storage/categoria/".$gallery->id_category."/".$input['imagename'];
        $gallery->copertina=$request->copertina;
        //dd($gallery);
        $gallery->save();
        $gallery->id;
        
        $galleries=gallery::all();
        $category=category::all();
        //return view('gallery.gallery',compact('galleries','category'));
        return redirect()->back()->with('message','Foto inserita correttamente');
    }

    /**
     * Display the specified resource.
     */
    public function show(gallery $gallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(gallery $gallery)
    {
        //dd($gallery->id_category);
        $oldcopertina=gallery::where('id_category',$gallery->id_category)->where('copertina',1)->update(['copertina'=>2]);
        $newcopertina=gallery::where('id',$gallery->id)->update(['copertina'=>1]);

        return redirect()->back()->with('message','copertina galleria '.$gallery->category->name.' aggiornata!');
        
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, gallery $gallery)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(gallery $gallery)
    {
        
        $delete=gallery::where('id',$gallery->id)->delete();

        return redirect()->back()->with('message','Foto cancellata correttamente');
    }
}
