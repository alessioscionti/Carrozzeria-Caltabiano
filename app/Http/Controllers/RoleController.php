<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $ruoli=Role::all();
        //dd($ruoli);
        return view('role.role',compact('ruoli'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $ruoli=Role::all();
       return view('role.create',compact('ruoli'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, Role $role)
    {
        $role->ruolo=$request->ruolo;
        $role->save();
        $ruoli=Role::all();
        return view('role.role',compact('ruoli'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Role $role, Request $request,User $user)
    {
        $ruolo=Role::find($role);
        
        return view('role.edit',compact('ruolo'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Role $role)
    {
        
        $role->ruolo=$request->ruolo;
        $role->save();
        $ruoli=Role::all();
        return view('role.role',compact('ruoli'))->with('message','Ruolo modificato');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Role $role,$id)
    {
        
        $role=Role::find($id)->delete();
        
        return redirect()->back()->with('message','Ruolo cancellato');
    }
}
