<?php

namespace App\Http\Controllers;

use App\Models\Calendary;
use Illuminate\Http\Request;
use Termwind\Components\Dd;

class CalendaryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $calendary=Calendary::all();
        return view('calendary.calendary',compact('calendary'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('calendary.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        
        $calendary=Calendary::create([
           'giorno'=>$request->giorni, 
           'apertura'=>$request->apertura, 
           'chiusura'=>$request->chiusura, 
           'visibile'=>$request->visibile, 
        ]);

        $calendary->save();

        return redirect()->back()->with('message','orario e giorno inseriti correttamente');
    }

    /**
     * Display the specified resource.
     */
    public function show(Calendary $calendary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Calendary $calendary)
    {
        return view('calendary.edit',compact('calendary'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Calendary $calendary)
    {
        /* dd($request->all()); */
        $updatecalendar=Calendary::where('id',$calendary->id)->update([
            'giorno'=>$request->giorni,
            'apertura'=>$request->apertura,
            'chiusura'=>$request->chiusura,
            'visibile'=>$request->visibile
        ]);
        return redirect()->back()->with('message','Giorno e orari aggiornati correttamente')->with(compact('calendary'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Calendary $calendary)
    {
        //
    }
}
