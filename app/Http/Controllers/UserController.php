<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Console\Input\Input;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users=User::with('role')->get();
        
        return view('users.users', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Role $role)
    {   
        $ruoli=Role::all();
        return view('users.create', compact('ruoli'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $controllomailesistente=User::where('email',$request->email)->get();
        
        if (!$controllomailesistente->isEmpty()) {
            return redirect()->back()->with('message','questa mail è gia presente nel sistema');
        }else {
            
            $user = new User();
            $user->name=$request->name;
            $user->email=$request->email;
            $user->password = Hash::make($request->password);
            $user->ruolo=$request->ruolo;
            $user->save();
            $users=User::all();
            return view('users.users',compact('users'));
        }



    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user,$id)
    {   
        
        $users=User::with('role')->where('id',$id)->get();
        
        $roles=Role::all();
        return view('users.edit',compact('users','roles'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request,User $user)
    {   
     
        
        
        $destinationPath="images/team/".$request->name;
        $user->name=$request->name;
        $user->ruolo=$request->role;
        $user->email=$request->email;
        if ($request->immagine) {
            $paths=Storage::putFileAs('team/'.$user->name.'/',$request->immagine,$user->name.'.png');
            /* $request->immagine->store(
                "images/team/".$user->name."/",
                
            ); */
        }
        $user->immagine ="storage/team/".$user->name."/".$user->name.".png";
        /* $immagine=$user->immagine;
        dd($immagine);
        $immagine->move($destinationPath, $request->immagine); */
        $user->skills=$request->skills;
        $user->descrizione=$request->descrizione;
        $user->in_team=$request->inteam;
        $user->save();

        $users=User::all();
        
        return view('users.users',compact('users'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
