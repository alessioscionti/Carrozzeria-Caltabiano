<?php

namespace App\Http\Controllers;

use App\Models\Pin;
use App\Models\Role;
use App\Models\User;
use App\Models\gallery;
use App\Models\category;
use App\Models\Calendary;
use Illuminate\Http\Request;

class PinController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    public function verificapin(Request $request){
        
        $ruoli=Role::all();
        $verifica=Pin::where('key',$request->password)->get();
        
        if (!$verifica->isEmpty()) {
            return view('auth.register',compact('ruoli'));
        }else{
            $users=User::with('role')->where('in_team',1)->get();
            $categorie=category::all();
            $gallery=gallery::where('copertina',1)->get();        
            $galleryall=gallery::all();
            $calendary=Calendary::all();

            return view('home', compact('users','gallery','galleryall','calendary'));

        }
    }
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Pin $pin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Pin $pin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Pin $pin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Pin $pin)
    {
        //
    }
}
