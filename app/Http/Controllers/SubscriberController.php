<?php

namespace App\Http\Controllers;

use App\Mail\Subscribe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SubscriberController extends Controller
{
    public function inviomail(Request $request){
        //dd($request->all());

        Mail::to($request->email)->send(new Subscribe($request));

        return redirect()->route('home','#messaggiook')->with('message','Email inviata con successo, il nostro staff ti contatterà il prima possibile')->with('contact',true);
    
    }
}
