<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\GeneralController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CalendaryController;
use App\Http\Controllers\PinController;
use App\Http\Controllers\SubscriberController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});
//GESTIONE UTENTI
Route::get('/users',[UserController::class,'index'])->name('users')->middleware(['auth', 'verified']);
Route::post('/editUser{id}',[UserController::class,'edit'])->name('users.edit')->middleware(['auth', 'verified']);
Route::post('/updateUser/{user}',[UserController::class,'update'])->name('users.update')->middleware(['auth', 'verified']);
Route::get('/userCreate',[UserController::class,'create'])->name('users.create')->middleware(['auth', 'verified']);
Route::post('/userStore',[UserController::class,'store'])->name('registra')->middleware(['auth', 'verified']);
Route::post('/verificapin',[PinController::class,'verificapin'])->name('verificapin');

//GESTIONE RUOLI
Route::get('/role',[RoleController::class,'index'])->name('role')->middleware(['auth', 'verified']);
Route::get('/roleCreate',[RoleController::class,'create'])->name('role.create')->middleware(['auth', 'verified']);
Route::post('/roleStore',[RoleController::class,'store'])->name('role.store')->middleware(['auth', 'verified']);
Route::post('/editRole{role}',[RoleController::class,'edit'])->name('role.edit')->middleware(['auth', 'verified']);
Route::post('/deleteRole{id}',[RoleController::class,'destroy'])->name('role.delete')->middleware(['auth', 'verified']);
Route::post('/updateRole/{role}',[RoleController::class,'update'])->name('role.update')->middleware(['auth', 'verified']);

//GESTIONE GALLERY
Route::get('/gallery',[GalleryController::class,'index'])->name('gallery')->middleware(['auth', 'verified']);
Route::get('/galleryCreate',[GalleryController::class,'create'])->name('gallery.create')->middleware(['auth', 'verified']);
Route::post('/galleryStore',[GalleryController::class,'store'])->name('gallery.store')->middleware(['auth', 'verified']);
Route::post('/editGallery{gallery}',[GalleryController::class,'edit'])->name('gallery.edit')->middleware(['auth', 'verified']);
Route::post('/deleteGallery{gallery}',[GalleryController::class,'destroy'])->name('gallery.delete')->middleware(['auth', 'verified']);

//GESTIONE CATEGORIE
Route::get('/category',[CategoryController::class,'index'])->name('category')->middleware(['auth', 'verified']);
Route::get('/categoryCreate',[CategoryController::class,'create'])->name('category.create')->middleware(['auth', 'verified']);
Route::post('/categoryStore',[CategoryController::class,'store'])->name('category.store')->middleware(['auth', 'verified']);
Route::post('/editcategory{category}',[CategoryController::class,'edit'])->name('category.edit')->middleware(['auth', 'verified']);
Route::post('/deletecategory{category}',[CategoryController::class,'destroy'])->name('category.delete')->middleware(['auth', 'verified']);

//GESTIONE CALENDARIO
Route::get('/calendary',[CalendaryController::class,'index'])->name('calendary')->middleware(['auth', 'verified']);
Route::get('/calendaryCreate',[CalendaryController::class,'create'])->name('calendary.create')->middleware(['auth', 'verified']);
Route::post('/calendaryStore',[CalendaryController::class,'store'])->name('calendary.store')->middleware(['auth', 'verified']);
Route::any('/editcalendary{calendary}',[CalendaryController::class,'edit'])->name('calendary.edit')->middleware(['auth', 'verified']);
Route::any('/deletecalendary{calendary}',[CalendaryController::class,'destroy'])->name('calendary.delete')->middleware(['auth', 'verified']);
Route::any('/updatecalendary{calendary}',[CalendaryController::class,'update'])->name('calendary.update')->middleware(['auth', 'verified']);

//GESTIONE MAIL
Route::post('/mail',[SubscriberController::class,'inviomail'])->name('email');








require __DIR__.'/auth.php';
