//import'bootstrap';
import'./general';
import.meta.glob([ '../images/**', ]);
import './ckeditor';
import'./main';
/* import'./imgal'; */
//import'./stacked';
import'./jquery.min';
//import'./jquery.counter';
import'./jquery.mb.YTPlayer';
import'./jquery.magnific-popup.min';
import'./scrollspy.min';
import'./jarallax';
import'./owl.carousel.min';
import'./waypoints.min';
//import'./index.min';
import'./function';

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();
