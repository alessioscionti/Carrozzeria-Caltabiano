<!DOCTYPE html>
<html lang="en" class="no-js">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aesthetic luxury fitness</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="Themesdesign" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
      integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
      crossorigin="anonymous">
    
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- Bootstrap core CSS -->
</head>
<style>
    .black{
    color: black;
    }
</style>
<body>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12">
                <h1 class="colorefirst fs-1">Hai ricevuto una nuova richiesta di contatto, di seguito i dettagli:</h1>
                <h2 class="black">Nome: <span class="colorefirst">{{$request->name}}</span></h2>
                <h2 class="black">email: <span class="colorefirst">{{$request->email}}</span></h2>
                <h2 class="black">oggetto: <span class="colorefirst">{{$request->subject}}</span></h2>
                <h2 class="black">Recapito telefonico: <span class="colorefirst">{{$request->subject}}</span></h2>
                <h2 class="black">Note: <span class="colorefirst">{{$request->comments}}</span></h2>
            </div>
        </div>
    </div>

</body>
<!-- JAVASCRIPTS -->
<script src="
https://cdn.jsdelivr.net/npm/scrollspy@1.6.0/index.min.js
"></script>
<script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script></body>
</html>