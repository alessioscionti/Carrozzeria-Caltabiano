<x-app-layout>
    <div class="container w-50">
        <div class="row justify-content-center">
            <div class="col-12">
                <form method="POST" action="{{ route('category.store') }}" enctype="multipart/form-data">
                    @csrf

        <!-- Name -->

                    <x-input-label for="name" :value="__('Nome categoria')" />
                    <select name="categorie" id="categorie" required autofocus>
                            <option value="" disabled selected>--</option>
                        @foreach ($categorie as $categoria)
                            <option value="{{$categoria->id}}">{{$categoria->name}}</option>
                        @endforeach
                            <option value="altro">Aggiungi categoria</option>
                    </select>
                        <div class="flex items-center justify-end mt-4">
                            <x-primary-button class="ml-4">
                                {{ __('salva') }}
                            </x-primary-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Email Address -->
        

    <script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
    <script>
        $('#categorie').change(function (e) { 
            e.preventDefault();
            if ($(this).val()=='altro') {
                $('#nuovonome').attr('hidden', false);
                $('#labelnome').attr('hidden', false);
            }else{
                $('#nuovonome').attr('hidden', true);
                $('#labelnome').attr('hidden', true);

            
            }
            
        });
    </script>
</x-app-layout>
