<x-app-layout>
    <div class="container-xxl" style="margin-top: 100px">
        <div class="row justify-content-center">
            <div class="col-6 text-center">
                <a href="{{route('calendary.create')}}"><p class="fas fa-plus-circle btn btn-success" style="color:white;font-size:1rem;margin-left:-7rem;"> Aggiungi o modifica giorni</p></a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <x-input-label class="colorefirst mt-5 fs-1" for="name" :value="__('Gestione Calendario')" />
                <table class="table table-dark table-responsive mt-5">
                    <thead>
                        <tr>
                            <th>Giorno</th>
                            <th>Apertura</th>
                            <th>Chiusura</th>
                            <th>Visibile</th>
                            <th>Azioni</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($calendary as $calendario)
                        <tr>
                            <td>{{$calendario->giorno}}</td>
                            <td>{{$calendario->apertura}}</td>
                            <td>{{$calendario->chiusura}}</td>
                            <td>{{$calendario->visibile}}</td>
                            <td><form action="{{route('calendary.edit',$calendario)}}" method="POST">
                                @csrf
                                <div class="flex items-center justify-content-center">
                                    <x-primary-button class="ml-4">
                                        {{ __('Modifica') }}
                                    </x-primary-button>
                                </div>
                            </form></td>
                        </tr> 
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


</x-app-layout>