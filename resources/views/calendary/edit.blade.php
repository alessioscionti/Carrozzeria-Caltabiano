<x-app-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
    </div>
    <form action="{{route('calendary.update',$calendary)}}" method="get">
        @csrf
        <div class="container w-25 mt-5">
            <div class="row justify-content-center">
                <div class="col-12">
                    <select name="giorni" id="giorni">
                        <option value="{{$calendary->giorno}}">{{$calendary->giorno}}</option>
                        <option value="{{$calendary->giorno}}">{{$calendary->giorno}}</option>
                        <option value="{{$calendary->giorno}}">{{$calendary->giorno}}</option>
                        <option value="{{$calendary->giorno}}">{{$calendary->giorno}}</option>
                        <option value="{{$calendary->giorno}}">{{$calendary->giorno}}</option>
                        <option value="{{$calendary->giorno}}">{{$calendary->giorno}}</option>
                        <option value="{{$calendary->giorno}}">{{$calendary->giorno}}</option>
                    </select>
                </div>
                <div class="col-12">
                    <x-input-label class="colorefirst fs-5" for="apertura" :value="__('Orario di apertura')"/>
                    <x-text-input id="name" class="block mt-1 w-full" type="text" name="apertura" value="{{$calendary->apertura}}" required autofocus autocomplete="apertura" />
                </div>
                <div class="col-12">
                    <x-input-label class="colorefirst fs-5" for="chiusura" :value="__('Orario di chiusura')"/>
                    <x-text-input id="name" class="block mt-1 w-full" type="text" name="chiusura" value="{{$calendary->chiusura}}" required autofocus autocomplete="chiusura" />
                </div>
                <div class="col-12">
                    <x-input-label class="colorefirst fs-5" for="visibile" value="Vuoi rendere visibile il giorno sul sito?"/>
                    <select name="visibile" id="visibile">
                        @if ($calendary->visibile==1)
                        <option value="0">No</option>
                        <option value="1" selected>Si</option>
                        @else
                        <option value="0" selected>No</option>
                        <option value="1">Si</option>
                        @endif
                    </select>
                </div>
                <div class="flex items-center justify-end mt-4">
                    <x-primary-button class="ml-4">
                        {{ __('salva') }}
                    </x-primary-button>
                </div>
            </div>
        </div>
    </form>



</x-app-layout>