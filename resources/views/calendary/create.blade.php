<x-app-layout>
    <form action="{{route('calendary.store')}}" method="POST">
        @csrf
        <div class="container w-25 mt-5">
            <div class="row justify-content-center">
                <div class="col-12">
                    <select name="giorni" id="giorni">
                        <option value="Lunedi">Lunedi</option>
                        <option value="Martedi">Martedi</option>
                        <option value="Mercoledi">Mercoledi</option>
                        <option value="Giovedi">Giovedi</option>
                        <option value="Venerdi">Venerdi</option>
                        <option value="Sabato">Sabato</option>
                        <option value="Domenica">Domenica</option>
                    </select>
                </div>
                <div class="col-12">
                    <x-input-label class="colorefirst fs-5" for="apertura" :value="__('Orario di apertura')"/>
                    <x-text-input id="name" class="block mt-1 w-full" type="text" name="apertura" :value="old('apertura')" required autofocus autocomplete="apertura" />
                </div>
                <div class="col-12">
                    <x-input-label class="colorefirst fs-5" for="chiusura" :value="__('Orario di chiusura')"/>
                    <x-text-input id="name" class="block mt-1 w-full" type="text" name="chiusura" :value="old('chiusura')" required autofocus autocomplete="chiusura" />
                </div>
                <div class="col-12">
                    <x-input-label class="colorefirst fs-5" for="visibile" :value="__('Vuoi rendere visibile il giorno sul sito?')"/>
                    <select name="visibile" id="visibile">
                        <option value="0">No</option>
                        <option value="1">Si</option>
                    </select>
                </div>
                <div class="flex items-center justify-end mt-4">
                    <x-primary-button class="ml-4">
                        {{ __('salva') }}
                    </x-primary-button>
                </div>
            </div>
        </div>
    </form>
</x-app-layout>
