<x-app-layout>



    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h2 class="colorefirst uppercase" style="font-size: 2rem;">scegli cosa fare</h2>
            </div>
            <hr class="hr mt-5 mb-8">
        </div>
    </div>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 text-center mt-5">
                <a href="{{Route('gallery')}}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">Gestisci gallery</a>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 text-center mt-5">
                <a href="{{Route('users')}}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">Gestisci utenti</a>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 text-center mt-5">
                <a href="{{Route('role')}}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">Gestisci Ruoli</a>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 text-center mt-5">
                <a href="{{Route('calendary')}}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full">Gestisci Calendario</a>
            </div>
        </div>
    </div>
    
    
</x-app-layout>
