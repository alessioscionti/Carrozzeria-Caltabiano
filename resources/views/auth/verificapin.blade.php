<x-guest-layout>
    <form method="POST" action="{{ route('verificapin') }}">
        @csrf
        <!-- Password -->
        <div class="mt-4">
            <x-input-label for="password" :value="__('Password')" />

            <x-text-input id="password" class="block mt-1 w-full"
                            type="password"
                            name="password"
                            required autocomplete="new-password" />

            <x-input-error :messages="$errors->get('password')" class="mt-2" />
        </div>
        <div class="flex items-center justify-end mt-4">
            <x-primary-button class="ml-4">
                {{ __('Invia') }}
            </x-primary-button>
        </div>
    </form>
</x-guest-layout>
