<x-app-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
    </div>
    <div class="container-xxl" style="margin-top: 100px">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <a href="{{route('role.create')}}"><p class="fas fa-plus-circle btn btn-success" style="color:white;font-size:1rem;margin-left:-7rem;"> c r e a &nbsp; n u o v o &nbsp; R u o l o</p></a>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row justify-content-center">
            @foreach ($ruoli as $ruolo)
            <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 text-center mt-5">
                @csrf
                <div class="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                    <div class="p-5">
                        <a href="#">
                            <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-black">{{$ruolo->ruolo}}</h5>
                        </a>
                    </div>
                    <div class="container mb-5">
                        <div class="row justify-content-center">
                            @if ($ruolo->id==1)
                            <div class="col-12">
                                <form action="{{route('role.edit',$ruolo)}}" method="POST">
                                    @csrf
                                    <button class="btn btn-dark">Modifica</button>
                                </form>
                            </div>
                            
                            @else
                            <div class="col-6">
                                <form action="{{route('role.edit',$ruolo)}}" method="POST">
                                    @csrf
                                    <button class="btn btn-dark">Modifica</button>
                                </form>
                            </div>
                            <div class="col-6">
                                <form action="{{route('role.delete',$ruolo->id)}}" method="POST">
                                    @csrf
                                    <button class="btn btn-danger">elimina</button>
                                </form>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    
</x-app-layout>