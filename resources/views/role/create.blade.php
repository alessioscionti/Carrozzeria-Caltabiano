<x-app-layout>
    <form method="POST" action="{{ route('role.store') }}">
        @csrf

        <!-- Name -->
        <div>
            <x-input-label for="name" :value="__('Name')" />
            <x-text-input id="name" class="block mt-1 w-full" type="text" name="ruolo" :value="old('name')" required autofocus autocomplete="name" />
            <x-input-error :messages="$errors->get('name')" class="mt-2" />
        </div>

        <!-- Email Address -->
        

        <div class="flex items-center justify-end mt-4">
            <x-primary-button class="ml-4">
                {{ __('salva') }}
            </x-primary-button>
        </div>
    </form>
</x-app-layout>
