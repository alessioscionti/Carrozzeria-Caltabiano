<nav class="navbar navbar-expand-lg fixed-top navbar-custom navbar-light sticky" id="inizio">
    <div class="container">
        <a class="navbar-brand" href="#"><span style="color:goldenrod">Aesthetic luxury fitness</span></a>
        <button class="navbar-toggler collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false">
            <span class="mdi mdi-menu"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarCollapse" style="visibility:visible!important;">
            <ul class="navbar-nav ml-auto" id="menunav">
                <li class="nav-item active">
                    <a class="nav-link" href="#home">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#chisiamo">Chi siamo</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#gallery">gallery</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#struttura">Struttura</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#team">Team</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#servizi">Servizi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#contact">Contact</a>
                </li>
            </ul>
            <div>
                <ul class="top-right text-right list-unstyled list-inline mb-0 nav-social">
                    <li class="list-inline-item"><a href="" class="facebook"><i class="mdi mdi-facebook"></i></a></li>
                    <li class="list-inline-item"><a href="" class="twitter"><i class="mdi mdi-twitter"></i></a></li>
                    <li class="list-inline-item"><a href="" class="instagram"><i class="mdi mdi-instagram"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>
