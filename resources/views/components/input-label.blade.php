@props(['value'])

<label {{ $attributes->merge(['class' => 'block font-medium text-sm text-gray-700']) }}>
    <span style="font-weight: 900">{{ $value ?? $slot }}</span>
</label>
