<x-guest-layout>
    @foreach ($ruolo as $role)
        
    
    <form action="{{ route('role.update',compact('role')) }}"method="post" >
        @csrf
        
        <!-- Name -->
        <div>
            <x-input-label for="name" :value="__('Name')" />
            <x-text-input id="name" class="block mt-1 w-full" type="text" name="ruolo" :value="old('ruolo')" required autofocus autocomplete="name" />
            <x-input-error :messages="$errors->get('name')" class="mt-2" />
        </div>
        <div class="flex items-center justify-end mt-4">
            <x-primary-button class="ml-4">
                {{ __('salva') }}
            </x-primary-button>
        </div>
    </form>
    @endforeach
</x-guest-layout>
