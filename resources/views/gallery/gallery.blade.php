<x-app-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
    </div>
    {{-- GESTIONE CATEGORIE --}}
    <div class="container-xxl" style="margin-top: 100px">
        <div class="row justify-content-center">
            <div class="col-6 text-center">
                <button id="category" class="fas fa-plus-circle btn btn-success"><p style="color:white;"> Gestisci Categorie </p></button>
            </div>
            <div class="col-6 text-center">
                <a href="{{route('gallery.create')}}"><p class="fas fa-plus-circle btn btn-success" style="color:white;font-size:1rem;margin-left:-7rem;"> Aggiungi Immagine</p></a>
            </div>
        </div>
    </div>
    <div class="container mt-5" >
        <div class="row justify-content-center">
            <div class="col-12 text-center"id="tabcat" style="display:none">
                <table class="table table-dark rounded" >
                    <thead>
                        <tr>
                            <th>Nome categoria</th>
                            <th>Azioni</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($category as $cat)
                        <tr>
                            <td>{{$cat->name}}</td>
                            <td>
                                <button class="btn btn-dark">Modifica</button>
                                <button class="btn btn-dark">Elimina</button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <button class="btn btn-custom" id="aggiungicat">Aggiungi Categoria</button>
                <div class="container w-50" id="newcatform" style="display:none">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <form method="POST" action="{{ route('category.store') }}" enctype="multipart/form-data">
                                @csrf
            
                    <!-- Name -->
            
                                <x-input-label for="name" :value="__('Nome categoria')" />
                                <x-text-input  class="block mt-1 w-full" id="name" type="text" name="name" :value="old('name')" autofocus autocomplete="name" placeholder="inserisci nome categoria"/>
                                <div class="flex items-center justify-end mt-4">
                                        <x-primary-button class="ml-4">
                                            {{ __('salva') }}
                                        </x-primary-button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
    {{-- FINE GESTIONE CATEGORIE --}}

    {{-- GESTIONE GALLERY E FOTO --}}
    <div class="container-fluid">
        <div class="row justify-content-center">
                @foreach ($galleries as $gallery => $values)
                <div class="col-12 mt-5 text-center">
                    <h2 class="text-start m-3 bg-danger p-2 rounded text-center"><span class="colorefirst">Galleria: </span><span class="text-white">{{$gallery}}</span></h2>
                    <div class="container">
                        <div class="row justify-content-center">
                            @foreach ($values as $value)
                            <div class="galleryItem card col-12 col-sm-12 col-md-12 col-lg-3">
                                @if ($value->copertina==1)
                                <div class="ribbon">
                                    <span>COPERTINA</span>
                                </div>
                                @endif
                                <img src="{{asset(''.$value->path.'') }}" class="img-fluid" style="width:100%;height:358px">
                                <figcaption class="box__caption container-fluid">
                                        <div class="row justify-content-center">
                                            <div class="col-5 text-center">
                                                <form action="{{route('gallery.delete',$value->id)}}" method="POST">
                                                    @csrf
                                                    <button class="btn btn-danger" style="font-size: 0.7rem;">elimina</button>
                                                </form>
                                            </div>
                                            <div class="col-5 text-center">
                                                <form action="{{route('gallery.edit',$value->id)}}" method="POST">
                                                    @csrf
                                                    <button class="btn btn-danger" style="font-size: 0.7rem;">copertina</button>
                                                </form>
                                            </div>
                                        </div>
                            </figcaption>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        {{-- FINE GALLERY E FOTO --}}

        <script>
            $('#category').click(function (e) { 
                e.preventDefault();
                $('#tabcat').toggle(400);
            });

            $('#aggiungicat').click(function (e) { 
                e.preventDefault();
                $('#newcatform').toggle(400);
            });
        </script>
    
</x-app-layout>