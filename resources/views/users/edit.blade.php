<x-app-layout>
    <script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
    <div class="container-xxl d-flex justify-content-center mt-5">
        <div class="row justify-content-center">
            <div class="col-12">
                @foreach ($users as $user)
                <form id='survey-form' method="POST" action="{{route('users.update',$user->id)}}" enctype="multipart/form-data">
                    @csrf
                <div class="bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                    <img class="rounded-t-lg img-fluid text-center" src="{{ asset('storage/app/'.$user->immagine) }}" alt="" />
                    <div class="p-5">
                        <x-input-label for="name" :value="__('Nome')" />
                        <input type="text" value="{{$user->name}}" class="block mt-1 w-full" name="name">
                        <hr class="hr mt-5 mb-5">
                        <x-input-label for="name" :value="__('Ruolo')" />
                        <select name="role" id="role">
                            <option value="{{$user->role->ruolo}}" disabled>{{$user->role->ruolo}}</option>
                            @foreach ($roles as $role)
                               <option value="{{$role->id}}">{{$role->ruolo}}</option> 
                            @endforeach
                        </select>
                        <hr class="hr mt-5 mb-5">
                        <x-input-label for="name" :value="__('Email')" />
                        <input type="text" value="{{$user->email}}" name="email">
                        <hr class="hr mt-5 mb-5">
                        <x-input-label for="name" :value="__('Immagine')" />
                        <input type="file" value="{{$user->immagine}}" name="immagine" style="width: 100%">
                        <hr class="hr mt-5 mb-5">
                        <x-input-label for="inteam" :value="__('Visualizzare nella Homepage?')" />
                        <select name="inteam" id="">
                            @if ($user->in_team==0)
                                <option value="0" disabled>NO (attuale)</option>
                                <option value="0" selected>NO</option>
                                <option value="1">SI</option>
                            @else
                                <option value="1" disabled>SI (attuale)</option>
                                <option value="0">NO</option>
                                <option value="1" selected>SI</option>
                            @endif
                        </select>
                        <hr class="hr mt-5 mb-5">
                        <form method="post" action="" enctype="multipart/form-data">
                            @csrf
                            <label for="skills">Inserisci skills {{$user->name}}</label>
                            <div class="form-group">
                                <textarea class="ckeditor form-control"  name="skills">{{$user->skills}}</textarea>
                            </div>
                            <label for="skills">Inserisci Descrizione {{$user->name}}</label>
                            <div class="form-group">
                                <textarea class="ckeditor form-control"  name="descrizione">{{$user->descrizione}}</textarea>
                            </div>
                        </form>

                        <hr class="hr mt-5 mb-5">
                        {{-- <form method="post" action="" enctype="multipart/form-data">
                            @csrf
                            <label for="descrizione">Inserisci descrizione {{$user->name}}</label>
                            <div class="form-group">
                                <textarea class="ckeditor form-control"  name="descrizione">{{$user->descrizione}}</textarea>
                            </div>
                        </form> --}}
                        <button class="btn btn-success mt-2">Salva</button>
                    </div>
                </div>
                </form>
                @endforeach
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.ckeditor').ckeditor();
        });

    </script>
</x-app-layout>

{{-- <x-app-layout>
    <div class="container-fluid mt-5">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                @foreach ($users as $user)
                <form id='survey-form' method="POST" action="{{route('users.update',$user->id)}}" enctype="multipart/form-data">
                    @csrf
                <h2 class="text-white">{{$user->name}}</h2>
                <div class="container-xxl d-flex justify-content-center text-center">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <img class="rounded-t-lg img-fluid text-center" src="{{ asset('storage/app/'.$user->immagine) }}" width="300" height="400" alt="" />
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <label for="name" class="text-center mt-2">Nome</label><br>
                            <input type="text" value="{{$user->name}}" name="name"><br>
                            <label for="role" class="text-center mt-2">Ruolo</label><br>
                            <select name="role" id="role">
                                <option value="{{$user->role->ruolo}}" disabled>{{$user->role->ruolo}}</option>
                                @foreach ($roles as $role)
                                <option value="{{$role->id}}">{{$role->ruolo}}</option> 
                                @endforeach
                            </select><br>
                            <label for="email" class="text-center mt-2">Email</label><br>
                            <input type="text" value="{{$user->email}}" name="email"><br>
                            <label for="immagine" class="text-center mt-2">Immagine</label><br>
                            <input type="file" value="{{$user->immagine}}" name="immagine"><br>
                            <form method="post" action="" enctype="multipart/form-data">
                                @csrf
                                <label for="skills">Inserisci skills {{$user->name}}</label>
                                <div class="form-group">
                                    <textarea class="ckeditor form-control"  name="skills">{{$user->skills}}</textarea>
                                </div>
                        </div>
                    </div>
                </div>
                </form>
                @endforeach
            </div>
        </div>
    </div>
</x-app-layout> --}}