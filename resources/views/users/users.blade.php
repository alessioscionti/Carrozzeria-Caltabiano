<x-app-layout>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
            @if (Session::has('message'))
                <div class="alert alert-success">
                    {{session('message')}}
                </div>
            @endif
            </div>
        </div>
    </div>
    <div class="container" style="margin-top: 100px">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <a href="{{route('users.create')}}"><p class="fas fa-plus-circle btn btn-success" style="color:white;font-size:1rem;margin-left:-7rem;"> c r e a &nbsp; n u o v o &nbsp; U t e n t e </p></a>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row justify-content-center">
            @foreach ($users as $user)
            <div class="col-12 col-xs-12 col-md-12 col-lg-3 col-xl-3 text-center mt-3">
                <form action="{{route('users.edit',$user)}}" method="POST">
                @csrf
                <div class="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                    <a href="#">
                        <img class="rounded-t-lg" src="{{asset("".$user->immagine."") }}" alt="" />
                    </a>
                    <div class="p-5">
                        <a href="#">
                            <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-black">{{$user->name}}</h5>
                        </a>
                        <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">Email:{{$user->email}}</p>
                        <p class="mb-3 font-normal text-gray-700 dark:text-gray-400">Ruolo:{{$user->role->ruolo}}</p>
                        <label for="skill">Skills</label>
                        <ul>
                            {!!$user->skills!!}
                        </ul>
                        
                        <button class="btn btn-dark">Modifica</button>
                    </div>
                </div>
                </form>
            </div>
            @endforeach
        </div>
    </div>
    
</x-app-layout>