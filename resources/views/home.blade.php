<x-layout>
    <style>
        .card{
        border: none!important;
        margin-top: 1rem;
        }
    </style>
<section class="section bg-home" id="home">
    <div class="bg-overlay"></div>
    <div class="home-center mt-5">
        <div class="home-desc-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-5">
                        <div class="text-center text-white">
                            <img src="{{asset("images/aesthetic_l.png") }}" class="img-fluid">
                            {{-- <img src="'resources/images/aesthetic.png'" class="img-fluid"> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mouse_down text-center">
        <a href="#contact" class="text-white scroll">
            <i class="mdi mdi-arrow-down scroll-icon play-icon"></i>
        </a>
    </div>

</section>

<!-- END HOME -->
<section class="section heigth-mobile" id="chisiamo">
    <div class="container-fluid mt-5 reveal">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h2 class="colorefirst fs-1 uppercase text-center">CHI SIAMO</h2>
            </div>
            <div class="col text-center text-base border-corners mt-5 align-middle">
                <p class="chisiamo text-sm-left">Aesthetic Luxury Fitness Club
                    è un sogno che si realizza.
                    È un club nato dal desiderio di creare uno spazio esclusivo per far sentire unici i nostri soci.
                    È il luogo in cui ci prendiamo cura del tuo benessere fisico, accompagnandoti verso il raggiungimento dei tuoi obiettivi.
                    Questo è reso possibile dalla professionalità che il club mette a disposizione, offrendo servizi quali personal trainer, consulenze private e preparazioni atletiche, attraverso attrezzature all’avanguardia e innovative, di ultimissima generazione, e al personale altamente qualificato con esperienza pluriennale nel settore.
                    Aesthetic Luxury Fitness Club non è solo sala attrezzi, qui troverai un’area specializzata in cui potrai svolgere l’attività di functional training e only woman, un allenamento dedicato esclusivamente alla donna che ha lo scopo di rafforzare il core e tonificare gambe e glutei.</p>
            </div>
        </div>
    </div>
</section>
{{-- START GALLERY --}}
<section class="section" id="gallery">


<div class="container-fluid reveal">
    <div class="row justify-content-center">
        <h2 class="font-900 text-center"><span class="colorefirst fs-1 uppercase">Gallery</span></h2>
        <div class="flex-1">
            <div class="card-columns gallery">
                @foreach ($gallery as $item)
                <div class="galleryItem card m-0">
                    <!-- just need a img tag. if you are want a text add data-text-->
                    <img src="{{asset("".$item->path."") }}" class="img-fluid imagegallery">
                    <figcaption class="box__caption"><p class="colorefirst fs-1">{{$item->category->name}}</p></figcaption>
                </div>
                @endforeach
        </div>
    </div>
</div>

</section>
{{-- END GALLERY --}}
<!-- START SERVICES -->
<section class="section bg-light" id="struttura" >
    <div class="container reveal fade-left">
        <div class="row justify-content-center text-center">
            <div class="col-lg-12">
                <h2 class="font-900"><span class="colorefirst fs-1 uppercase">Struttura</span></h2>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="services-box mt-5">
                    <div class="services-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 640 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><style>svg{fill:#daa520}</style><path d="M96 64c0-17.7 14.3-32 32-32h32c17.7 0 32 14.3 32 32V224v64V448c0 17.7-14.3 32-32 32H128c-17.7 0-32-14.3-32-32V384H64c-17.7 0-32-14.3-32-32V288c-17.7 0-32-14.3-32-32s14.3-32 32-32V160c0-17.7 14.3-32 32-32H96V64zm448 0v64h32c17.7 0 32 14.3 32 32v64c17.7 0 32 14.3 32 32s-14.3 32-32 32v64c0 17.7-14.3 32-32 32H544v64c0 17.7-14.3 32-32 32H480c-17.7 0-32-14.3-32-32V288 224 64c0-17.7 14.3-32 32-32h32c17.7 0 32 14.3 32 32zM416 224v64H224V224H416z"/></svg>                    </div>
                    <div class="mt-3">
                        <p class="services-title mb-1">Sala attrezzi</p>
                        <div class="services-border"></div>
                        <p class="services-subtitle text-muted mt-3">

                            La nostra sala attrezzi è progettata per soddisfare le esigenze di ogni cliente. 
                            Puoi allenarti in modo sicuro ed efficace, raggiungendo i tuoi obiettivi di fitness.
                            
                            Dotata di attrezzature di <span class="colorefirst uppercase">alta qualità</span> da marchi leader del settore.
                                                       
                            Offriamo una varietà di <span class="colorefirst uppercase">servizi personalizzati</span> per aiutarti a raggiungere i tuoi obiettivi di fitness. I nostri personal trainer sono qualificati per sviluppare programmi di allenamento personalizzati che si adattano alle tue esigenze e al tuo livello di fitness. I nostri nutrizionisti possono aiutarti a creare un piano alimentare sano per sostenere il tuo programma di allenamento. E i nostri coach possono aiutarti a rimanere motivato e in pista.
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="services-box mt-5">
                    <div class="services-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><style>svg{fill:#daa520}</style><path d="M64 131.9C64 112.1 80.1 96 99.9 96c9.5 0 18.6 3.8 25.4 10.5l16.2 16.2c-21 38.9-17.4 87.5 10.9 123L151 247c-9.4 9.4-9.4 24.6 0 33.9s24.6 9.4 33.9 0L345 121c9.4-9.4 9.4-24.6 0-33.9s-24.6-9.4-33.9 0l-1.3 1.3c-35.5-28.3-84.2-31.9-123-10.9L170.5 61.3C151.8 42.5 126.4 32 99.9 32C44.7 32 0 76.7 0 131.9V448c0 17.7 14.3 32 32 32s32-14.3 32-32V131.9zM256 352a32 32 0 1 0 0-64 32 32 0 1 0 0 64zm64 64a32 32 0 1 0 -64 0 32 32 0 1 0 64 0zm0-128a32 32 0 1 0 0-64 32 32 0 1 0 0 64zm64 64a32 32 0 1 0 -64 0 32 32 0 1 0 64 0zm0-128a32 32 0 1 0 0-64 32 32 0 1 0 0 64zm64 64a32 32 0 1 0 -64 0 32 32 0 1 0 64 0zm32-32a32 32 0 1 0 0-64 32 32 0 1 0 0 64z"/></svg>                    </div>
                    <div class="mt-3">
                        <p class="services-title mb-1">Spogliatoi</p>
                        <div class="services-border"></div>
                        <p class="services-subtitle text-muted mt-3"> <span class="colorefirst uppercase"> Ambiente pulito e sicuro <br></span>

                            Manteniamo il nostro spogliatoio pulito e sicuro per garantire che i nostri clienti si sentano a proprio agio. I nostri bagni vengono puliti regolarmente e abbiamo un sistema di sicurezza per proteggere i tuoi effetti personali.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="services-box mt-5">
                    <div class="services-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 640 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><style>svg{fill:#daa520}</style><path d="M155.6 17.3C163 3 179.9-3.6 195 1.9L320 47.5l125-45.6c15.1-5.5 32 1.1 39.4 15.4l78.8 152.9c28.8 55.8 10.3 122.3-38.5 156.6L556.1 413l41-15c16.6-6 35 2.5 41 19.1s-2.5 35-19.1 41l-71.1 25.9L476.8 510c-16.6 6.1-35-2.5-41-19.1s2.5-35 19.1-41l41-15-31.3-86.2c-59.4 5.2-116.2-34-130-95.2L320 188.8l-14.6 64.7c-13.8 61.3-70.6 100.4-130 95.2l-31.3 86.2 41 15c16.6 6 25.2 24.4 19.1 41s-24.4 25.2-41 19.1L92.2 484.1 21.1 458.2c-16.6-6.1-25.2-24.4-19.1-41s24.4-25.2 41-19.1l41 15 31.3-86.2C66.5 292.5 48.1 226 76.9 170.2L155.6 17.3zm44 54.4l-27.2 52.8L261.6 157l13.1-57.9L199.6 71.7zm240.9 0L365.4 99.1 378.5 157l89.2-32.5L440.5 71.7z"/></svg>
                    </div>
                    <div class="mt-3">
                        <p class="services-title mb-1">Area Relax</p>
                        <div class="services-border"></div>
                        <p class="services-subtitle text-muted mt-3">La nostra area relax è un luogo dove rilassarsi e ricaricarsi dopo un allenamento.
                            Nella nostra area relax puoi preparare le tue bevande energetiche fresche e salutari. Abbiamo una varietà di ingredienti a tua disposizione, tra cui frutta, verdura, proteine e vitamine.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END SERVICES -->

<!-- START FEATURES -->
<section class="section" id="team" >
    <div class="container reveal fade-right">
        <div class="row justify-content-center text-center">
            <div class="col-lg-12">
                <h2 class="font-900"><span class="colorefirst fs-1 uppercase">Il nostro Team</span></h2>
                <p class="text-muted mt-4 title-subtitle mx-auto">Un team di professionisti dedicati al tuo benessere

                    Il nostro team è composto da professionisti del fitness qualificati e appassionati che sono dedicati al tuo benessere. Siamo qui per aiutarti a raggiungere i tuoi obiettivi di fitness, indipendentemente dal tuo livello di forma fisica o esperienza.</p>
            </div>
        </div>
        <div id="owl-demo" class="owl-carousel">
            @foreach ($users as $user)
            <div class="row pt-5 vertical-content">
                <div class="col-12 col-xs-12 col-md-6 col-lg-6 col-xl-6">
                    <div class="features-img">
                        <img src="{{asset("".$user->immagine."") }}" class="img-fluid">
                        {{-- <img src="resources/images/raciti.png" class="img-fluid"> --}}
                    </div>
                </div>
                <div class="col-12 col-xs-12 col-md-6 col-lg-6 col-xl-6">
                    <div class="features-contant">
                        <div class="features-heading colorefirst">{{$user->name}}</div>
                        <span class="fearures-desc text-muted mt-4">{!!$user->descrizione!!}</span>
                        <div class="mt-4">
                            <span class="colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>{!!$user->skills!!}</span>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <hr class="hr mt-5">
        
        <div class="row pt-5 vertical-content">
            <div class="col-12 text-center">
                <h2 class="font-900"><span class="colorefirst fs-1 uppercase">I nostri servizi</span></h2>
            </div>
            <div class="row pt-5 vertical-content reveal fade-left">
                <div class="col-md-6 mt-5">
                    <div class="float-sm-start">
                        <img src="{{asset("images/PERSONAL_TRAINER.png") }}" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-6 mt-5">
                    <div class="features-contant">
                        {{-- <div class="features-heading colorefirst">PERSONAL TRAINER</div> --}}
                        <p class="fearures-desc text-muted mt-4">Lezioni one-to-one con un tecnico altamente specializzato che tramite un percorso di allenamento personalizzato permette il raggiungimento dell’obiettivo richiesto da parte del cliente, non solo in termini di salute, ma anche di estetica o prestazione. </p>
                        <div class="mt-4">
                            <p class="fearures-desc text-muted mt-4">I benefici del Personal trainer</p>
                            <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Piano alimentare personalizzato </p>
                            <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Piano di allenamento personalizzato</p>
                            <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Supervisione del vostro allenamento</p>
                            <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Check fisici periodici</p>
                            <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Incremento dei risultati</p>
                        </div>
                        
                    </div>
                </div>
            </div>
            <hr class="hr mt-5">
        <div class="row pt-5 vertical-content mobile-reverse reveal fade-right">
            <div class="col-md-6 mt-5">
                <div class="features-contant">
                    {{-- <div class="features-heading colorefirst">FUNCTIONAL METABOLIC</div> --}}
                    <p class="fearures-desc text-muted mt-4">L’allenamento funzionale mira a ottimizzare il metabolismo attraverso esercizi ad alto impatto cardio-vascolare con l’obiettivo di aumentare la massa magra a discapito di quella grassa. Consigliato per il dimagrimento.
                    </p>
                    <div class="mt-4">
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>rafforzare il corpo nella sua totalità </p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>migliorare la postura</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>tonificare i muscoli</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>stimolare la reattività</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>intensificare la capacità cardio-respiratoria</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>favorire il dimagrimento e il consumo di calorie</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mt-5">
                <div class="float-sm-start">
                    <img src="{{asset('images/FUNCTIONAL_METABOLIC.png') }}" class="img-fluid">
                </div>
            </div>
        </div>
        <hr class="hr mt-5">
        <div class="row pt-5 vertical-content reveal fade-left">
            <div class="col-md-6 mt-5">
                <div class="float-sm-start">
                    <img src="{{asset('images/FUNCTIONAL_ADVANCE.png') }}" class="img-fluid">
                </div>
            </div>
            <div class="col-md-6 mt-5">
                <div class="features-contant">
                    {{-- <div class="features-heading colorefirst">FUNCTIONAL ADVANCE</div> --}}
                    <p class="fearures-desc text-muted mt-4">Simile a functional metabolic ma dove viene enfatizzata l’alta intensità. 
                    </p>
                    <div class="mt-4">
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Migliora la postura.</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Aumenta la gamma di movimento.</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Brucia i grassi.</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Aumenta la resistenza.</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Arricchisce le prestazioni atletiche.</p>
                    </div>
                </div>
            </div>
        </div>
        <hr class="hr mt-5">
        <div class="row pt-5 vertical-content mobile-reverse reveal fade-right">
            
            <div class="col-md-6 mt-5">
                <div class="features-contant">
                    {{-- <div class="features-heading colorefirst">ONLY WOMAN</div> --}}
                    <p class="fearures-desc text-muted mt-4">Protocollo specifico che mira alla tonificazione di glutei e addome e a migliorarne l’impatto estetico. Attività utile a mantenere i muscoli tonici. È consigliato a tutte le persone che oltre modellare la parte inferiore del corpo, vogliono rafforzarla, oltre che perdere peso. 
                    </p>
                    <div class="mt-4">
                        <p class="fearures-desc text-muted mt-4">alcuni degli esercizi che possono essere inclusi in questo allenamento:</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Squat</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Affondi </p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Planche</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Push-up</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Crunch</p>
                    </div>
                    <p class="fearures-desc text-muted mt-4">Con questo allenamento, puoi tonificare i muscoli e migliorare la tua forma fisica senza mettere troppa massa muscolare.</p>
                    
                </div>
            </div>
            <div class="col-md-6 mt-5">
                <div class="float-sm-start">
                    <img src="{{asset('images/ONLY_WOMAN.png') }}" class="img-fluid">
                </div>
            </div>
        </div>
        <hr class="hr mt-5">
        <div class="row pt-5 vertical-content reveal fade-left">
            <div class="col-md-6 mt-5">
                <div class="float-sm-start">
                    <img src="{{asset('images/SMALL_GROUP.png') }}" class="img-fluid">
                </div>
            </div>
            <div class="col-md-6 mt-5">
                <div class="features-contant">
                    {{-- <div class="features-heading colorefirst">FUNCTIONAL ADVANCE</div> --}}
                    <p class="fearures-desc text-muted mt-4">Allenamento personalizzato a numero chiuso, che va da un minimo di 3 persone a un massimo di 8, diretto da un tecnico altamente specializzato. Ogni cliente avrà un allenamento specifico alle proprie esigenze. 
                    </p>
                    <div class="mt-4">
                        <p class="fearures-desc text-muted mt-4">Alcuni dei benefici del lavoro in gruppo:</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Riduzione sensibile dello stress.</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Più motivazione, ci si stimola a vicenda!</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Si solidificano i rapporti di amicizia… e di coppia!</p>
                    </div>
                </div>
            </div>
        </div>
        <hr class="hr">
        <div class="row pt-5 vertical-content reveal fade-right">
                <div class="col-md-12 mt-5">
                    <div class="float-start">
                        <img src="{{asset('images/coaching_online.PNG') }}" class="img-fluid">
                    </div>
                </div>
            <div class="col-md-6 mt-5">
                <div class="float-start">
                    <img src="{{asset('images/coaching_online.jpg') }}" class="img-fluid">
                </div>
            </div>

            <div class="col-md-6 mt-5 float-end">
                <div class="features-contant">
                    {{-- <div class="features-heading colorefirst">ONLY WOMAN</div> --}}
                    <p class="fearures-desc text-muted mt-4">AESTHETIC coaching online è un percorso a distanza che ha l’obiettivo di organizzare e pianificare allenamenti e alimentazione di chi necessita di essere seguito in un percorso personalizzato di miglioramento del proprio stato di forma.

                        AESTHETIC coaching online è rivolto a tutte quelle persone che desiderano essere allenate e seguite da noi, ma per motivi di distanza non possono raggiungere il nostro Club, possono comunque affidarsi al nostro staff di professionisti per la propria programmazione d’allenamento e alimentazione.
                         
                    </p>
                    <div class="mt-4">
                        <p class="fearures-desc text-muted mt-4">GLI OBIETTIVI CHE SI RAGGIUNGONO CON AESTHETIC COACHING ONLINE:
                        </p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Ricomposizione corporea</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Dimagrimento e tonificazione</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Aumento massa muscolare</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Bodybuilding</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Fitness generale</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Preparazione sportiva</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Consigli alimentari</p>
                        <p class="features-plan text-muted mb-2 colorefirst"><i class="mdi mdi-chevron-right text-custom mr-2"></i>Recupero funzionale</p>
                    </div>
                    <p class="fearures-desc text-muted mt-4">Invieremo a coloro che decideranno di essere seguiti a distanza, tutto il materiale necessario per svolgere al meglio il proprio lavoro: consigli alimentari e schede di allenamento con video illustrativi per essere seguiti accuratamente dai nostri personal trainer.</p>
                    
                </div>
            </div>
        </div>
        <hr class="hr mt-5">

    </div>
</section>
<!-- END FEATURES -->

<!-- START VIDEO SECTION -->
<section class="section bg-video jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('images/img-3.jpg');" id="video" >
    <div class="bg-overlay"></div>
    <div class="container reveal fade-left">
        <div class="row justify-content-center text-center">
            <div class="col-lg-12">
                <h3 class="text-white font-300 text-uppercase">Aesthetic Luxury Fitness</h3>
                <a href="{{asset("storage/video/video.mp4") }}" class="play-btn video-play-icon">
                    <i class="mdi mdi-play text-white"></i>
                </a>
            </div>
        </div>
    </div>
</section>
<!-- END VIDEO SECTION -->

<!-- START CLIENTS -->
<section class="section bg-light" id="Clendario" >
    <div class="container reveal">
        <div class="row justify-content-center text-center">
            <div class="col-lg-12">
                <h2 class="colorefirst fs-1">Giorni e Orari di apertura</h2>
                <div class="container">
                    <div class="row">
                        @foreach ($calendary as $calendario)
                        <div class="col-6 col-xs-6 col-sm-6 col-md-2 col-lg-2 col-xl-2">
                            <p class="mt-5">
                                <span class="colorefirst uppercase fs-2">{{$calendario->giorno}}:</span>
                                @if ($calendario->visibile==1)
                                <ul>
                                    <li><span class="text-white"> Apertura:</span> <span class="colorefirst">{{$calendario->apertura}}</span></li>
                                    <li><span class="text-white"> Chiusura: </span><span class="colorefirst">{{$calendario->chiusura}}</span></li>
                                </ul>
                                @else
                                <ul>
                                    <li><span class="text-white">CHIUSO</span></li>
                                </ul> 
                                @endif
                            </p>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END CLIENTS -->


<!-- START PRICE -->
{{-- <section class="section bg-light" id="price">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-12">
                <h2 class="font-weight-light">Our Pricing</h2>
                <p class="text-muted mt-4 title-subtitle mx-auto">It is a long established fact that a reader will be of a page when established fact looking at its layout.</p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-4">
                <div class="bg-white mt-3 price-box">
                    <div class="pricing-name text-center">
                        <h4>Deluxe</h4>
                        <span class="text-muted">Perfect For Freelancer</span>
                    </div>

                    <div class="plan-price text-center mt-4">
                        <h2 class="text-custom"><span class="dolar">$</span> 59 <span>Per/month</span></h2>
                    </div>

                    <div class="price-features mt-5 ml-4">
                        <p><i class="mdi mdi-check"></i> Bandwidth: <span class="font-weight-bold">1GB</span></p>
                        <p><i class="mdi mdi-check"></i> Onlinespace: <span class="font-weight-bold">500MB</span></p>
                        <p><i class="mdi mdi-close"></i> Support: <span class="font-weight-bold">No</span></p>
                        <p><i class="mdi mdi-check"></i> Domain: <span class="font-weight-bold">1</span></p>
                        <p><i class="mdi mdi-check"></i> Hidden Fees: <span class="font-weight-bold">No</span></p>
                    </div>
                    <div class="text-center mt-5">
                        <a href="#" class="btn btn-custom">Join Now</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="bg-white mt-3 price-box">
                    <div class="pricing-name text-center">
                        <h4>Economy</h4>
                        <span class="text-muted">Perfect For Freelancer</span>
                    </div>
                    <div class="plan-price text-center mt-4">
                        <h2 class="text-custom"><span class="dolar">$</span> 79 <span>Per/month</span></h2>
                    </div>
                    <div class="price-features mt-5 ml-4">
                        <p><i class="mdi mdi-check"></i> Bandwidth: <span class="font-weight-bold">2GB</span></p>
                        <p><i class="mdi mdi-check"></i> Onlinespace: <span class="font-weight-bold">1GB</span></p>
                        <p><i class="mdi mdi-check"></i> Support: <span class="font-weight-bold">Yes</span></p>
                        <p><i class="mdi mdi-check"></i> Domain: <span class="font-weight-bold">3</span></p>
                        <p><i class="mdi mdi-check"></i> Hidden Fees: <span class="font-weight-bold">No</span></p>
                    </div>
                    <div class="text-center mt-5">
                        <a href="#" class="btn btn-custom">Join Now</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="bg-white mt-3 price-box">
                    <div class="pricing-name text-center">
                        <h4>Ultimate</h4>
                        <span class="text-muted">Perfect For Freelancer</span>
                    </div>
                    <div class="plan-price text-center mt-4">
                        <h2 class="text-custom"><span class="dolar">$</span> 99 <span>Per/month</span></h2>
                    </div>
                    <div class="price-features mt-5 ml-4">
                        <p><i class="mdi mdi-check"></i> Bandwidth: <span class="font-weight-bold">3GB</span></p>
                        <p><i class="mdi mdi-check"></i> Onlinespace: <span class="font-weight-bold">2GB</span></p>
                        <p><i class="mdi mdi-check"></i> Support: <span class="font-weight-bold">Yes</span></p>
                        <p><i class="mdi mdi-check"></i> Domain: <span class="font-weight-bold">Unlimited</span></p>
                        <p><i class="mdi mdi-check"></i> Hidden Fees: <span class="font-weight-bold">No</span></p>
                    </div>
                    <div class="text-center mt-5">
                        <a href="#" class="btn btn-custom">Join Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!-- END PRICE -->

<!-- START FAQ -->
{{-- <section class="section" id="faq">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-12">
                <h2 class="font-weight-light">Frequently Asked Questions</h2>
                <p class="text-muted mt-4 title-subtitle mx-auto">It is a long established fact that a reader will be of a page when established fact looking at its layout.</p>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-lg-4">
                <div class="faq-box bg-light p-4 mt-4">

                    <div class="faq-icon">
                        <i class="mdi mdi-help"></i>
                    </div>
                    <h5 class="text-custom">01.</h5>
                    <h3 class="faq-title mt-3">What is Lorem Ipsum?</h3>
                    <p class="faq-ans text-muted mt-3 mb-0">The point of using Lorem Ipsum is that it has a more-or-less normal they distribution of letters opposed to using content here.</p>
                </div>

            </div>

            <div class="col-lg-4">
                <div class="faq-box bg-light p-4 mt-4">

                    <div class="faq-icon">
                        <i class="mdi mdi-help"></i>
                    </div>
                    <h5 class="text-custom">02.</h5>
                    <h3 class="faq-title mt-3">Why use Lorem Ipsum?</h3>
                    <p class="faq-ans text-muted mt-3 mb-0">The point of using Lorem Ipsum is that it has a more-or-less normal they distribution of letters opposed to using content here.</p>

                </div>
            </div>

            <div class="col-lg-4">
                <div class="faq-box bg-light p-4 mt-4">
                    <div class="faq-icon">
                        <i class="mdi mdi-help"></i>
                    </div>

                    <h5 class="text-custom">03.</h5>
                    <h3 class="faq-title mt-3">How many variations exist?</h3>
                    <p class="faq-ans text-muted mt-3 mb-0">The point of using Lorem Ipsum is that it has a more-or-less normal they distribution of letters opposed to using content here.</p>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="faq-box bg-light p-4 mt-4">
                    <div class="faq-icon">
                        <i class="mdi mdi-help"></i>
                    </div>

                    <h5 class="text-custom">04.</h5>
                    <h3 class="faq-title mt-3">Is safe use Lorem Ipsum?</h3>
                    <p class="faq-ans text-muted mt-3 mb-0">The point of using Lorem Ipsum is that it has a more-or-less normal they distribution of letters opposed to using content here.</p>

                </div>
            </div>

            <div class="col-lg-4">
                <div class="faq-box bg-light p-4 mt-4">
                    <div class="faq-icon">
                        <i class="mdi mdi-help"></i>
                    </div>

                    <h5 class="text-custom">05.</h5>
                    <h3 class="faq-title mt-3">When can be used?</h3>
                    <p class="faq-ans text-muted mt-3 mb-0">The point of using Lorem Ipsum is that it has a more-or-less normal they distribution of letters opposed to using content here.</p>

                </div>
            </div>

            <div class="col-lg-4">
                <div class="faq-box bg-light p-4 mt-4">
                    <div class="faq-icon">
                        <i class="mdi mdi-help"></i>
                    </div>

                    <h5 class="text-custom">06.</h5>
                    <h3 class="faq-title mt-3">What is Lorem Ipsum?</h3>
                    <p class="faq-ans text-muted mt-3 mb-0">The point of using Lorem Ipsum is that it has a more-or-less normal they distribution of letters opposed to using content here.</p>

                </div>
            </div>
        </div>

    </div>
</section> --}}
<!-- END FAQ -->

<!-- START ABOUT -->
{{-- <section class="section bg-light" id="about">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="about-title mx-auto text-center">
                    <h2 class="font-weight-light">A Digital web studio creating stunning  & Engaging online Experiences </h2>
                    <p class="text-muted pt-4">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis.</p>
                </div>
            </div>
        </div>
        <!-- FUN-FACTS -->
        <div class="row justify-content-center mt-5" id="counter">
            <div class="col-lg-3">
                <div class="text-muted text-center">
                    <div class="counter-icon">
                        <i class="mdi mdi-coffee-outline text-custom"></i>
                    </div>
                    <h3 class="counter-value font-weight-normal mt-2" data-count="1895">10</h3>
                    <p>Cups Of Coffee</p>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="text-muted text-center about-border-left">
                    <div class="counter-icon">
                        <i class="mdi mdi-emoticon-happy text-custom"></i>
                    </div>
                    <h3 class="counter-value font-weight-normal mt-2" data-count="485">1</h3>
                    <p>Happy Clients</p>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="text-muted text-center about-border-left">
                    <div class="counter-icon">
                        <i class="mdi mdi-thumb-up-outline text-custom"></i>
                    </div>
                    <h3 class="counter-value font-weight-normal mt-2" data-count="3561">1</h3>
                    <p>Project Completed</p>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="text-muted text-center about-border-left">
                    <div class="counter-icon">
                        <i class="mdi mdi-account-outline text-custom"></i>
                    </div>
                    <h3 class="counter-value font-weight-normal mt-2" data-count="87">1</h3>
                    <p>Smart Employee</p>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-3">
                <div class="mt-3">
                    <img src="images/team/img-1.jpg" alt="" class="img-fluid mx-auto d-block rounded img-thumbnail">
                    <div class="text-center mt-3">
                        <p class="font-weight-bold mb-0">Wade G. Wilhite</p>
                        <p class="text-muted mb-0">CEO/Founder</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="mt-3">
                    <img src="images/team/img-2.jpg" alt="" class="img-fluid mx-auto d-block rounded img-thumbnail">
                    <div class="text-center mt-3">
                        <p class="font-weight-bold mb-0">William S. Blay</p>
                        <p class="text-muted mb-0">CTO/Co-Founder</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="mt-3">
                    <img src="images/team/img-3.jpg" alt="" class="img-fluid mx-auto d-block rounded img-thumbnail">
                    <div class="text-center mt-3">
                        <p class="font-weight-bold mb-0">Matthew B. Morales</p>
                        <p class="text-muted mb-0">Web Designer</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="mt-3">
                    <img src="images/team/img-4.jpg" alt="" class="img-fluid mx-auto d-block rounded img-thumbnail">
                    <div class="text-center mt-3">
                        <p class="font-weight-bold mb-0">Luke L. Johnston</p>
                        <p class="text-muted mb-0">Web Developer</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!-- END ABOUT -->

<!-- START GET STARTED -->
{{-- <section class="section jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('images/img-2.jpg');">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="text-center cta-kivox mx-auto text-white">
                    <h1 class="cta-title mb-0">Let's get started with kivox</h1>
                    <p class="cta-subtitle mt-4 mb-4">Etiam sed.Interdum consequat proin vestibulum class at a euismod mus luctus quam.Lorem ipsum dolor sit amet, consectetur adipisicing eli.class at a euismod mus luctus quam.</p>
                    <div class="mt-4">
                        <a href="#" class="btn btn-outline-custom btn-round">Get Started</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!-- END GET STARTED -->

<!-- START BLOG -->
{{-- <section class="section bg-light" id="blog">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-lg-12">
                <h2 class="font-weight-light">Our Blog</h2>
                <p class="text-muted mt-4 title-subtitle mx-auto">It is a long established fact that a reader will be of a page when established fact looking at its layout.</p>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-4">
                <div class="blog-post mt-3">
                    <div>
                        <img src="images/blog/blog-1.jpg" alt="" class="img-fluid mx-auto d-block rounded">
                        <div class="blog-tag-box">
                            <a href=""><span class="blog-tag">Design</span></a>
                            <a href=""><span class="blog-tag">work</span></a>
                        </div>
                    </div>
                    <div class="blog-post-detail mt-4 p-1">
                        <div>

                            <p class="blog-date text-muted"><i class="mdi mdi-calendar-range"></i> 18 October, 2018 <i class="mdi mdi-message-reply-text ml-3"></i> 16 comments </p>
                            <h6 class=""><a href="#" class="text-dark">Donec vitae sapien venenatis faucibus...</a></h6>
                            <p class="text-muted mt-3 blog-desc">Lorem ipsum dolor amet consectetuer adipiscing aenean commodo ligula eget dolor Aenean massa cum natoque parturient montes.</p>
                            <div class="read-more">
                                <a href="">Read More <i class="mdi mdi-arrow-right ml-1"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blog-post mt-3">
                    <div>
                        <img src="images/blog/blog-2.jpg" alt="" class="img-fluid mx-auto d-block rounded">
                        <div class="blog-tag-box">
                            <a href=""><span class="blog-tag">Design</span></a>
                            <a href=""><span class="blog-tag">work</span></a>
                        </div>
                    </div>
                    <div class="blog-post-detail mt-4 p-1">
                        <div>
                            <p class="blog-date text-muted"><i class="mdi mdi-calendar-range"></i> 18 October, 2018 <i class="mdi mdi-message-reply-text ml-3"></i> 16 comments </p>
                            <h6 class=""><a href="#" class="text-dark">Nullam dictum felis pede mollis pretium...</a></h6>
                            <p class="text-muted mt-3 blog-desc">Lorem ipsum dolor amet consectetuer adipiscing aenean commodo ligula eget dolor Aenean massa cum natoque parturient montes.</p>
                            <div class="read-more">
                                <a href="">Read More <i class="mdi mdi-arrow-right ml-1"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="blog-post mt-3">
                    <div>
                        <img src="images/blog/blog-3.jpg" alt="" class="img-fluid mx-auto d-block rounded">
                        <div class="blog-tag-box">
                            <a href=""><span class="blog-tag">Design</span></a>
                            <a href=""><span class="blog-tag">work</span></a>
                        </div>
                    </div>
                    <div class="blog-post-detail mt-4 p-1">
                        <div>
                            <p class="blog-date text-muted"><i class="mdi mdi-calendar-range"></i> 18 October, 2018 <i class="mdi mdi-message-reply-text ml-3"></i> 16 comments </p>
                            <h6 class=""><a href="#" class="text-dark">Vestibulum fringilla pede sit amet augue...</a></h6>
                            <p class="text-muted mt-3 blog-desc">Lorem ipsum dolor amet consectetuer adipiscing aenean commodo ligula eget dolor Aenean massa cum natoque parturient montes.</p>
                            <div class="read-more">
                                <a href="">Read More <i class="mdi mdi-arrow-right ml-1"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!-- END BLOG -->

<!-- START CONTACT -->
{{-- GESTIONE MESSAGGIO DI SISTEMA MAIL OK--}}
<?php 
if (Session::has('message')) {
    $display="";
}else{
    $display="none";
}
?>
<section class="section" id="messaggiook" style="display:{{$display}}">
    <div class="container navbar-custom nav-sticky">
      <div class="row justify-content-center">
          <div class="col-12 text-center">
          @if (Session::has('message'))
              <div class="colorefirst fs-2">
                  {{session('message')}}
              </div>
          @endif
          </div>
      </div>
  </div>
</section>
{{-- FINE GESTIONE MESSAGGIO DI SISTEMA MAIL OK--}}
<section class="section" id="contact" >
    <div class="container reveal">
        <div class="row mt-5">
            <div class="col-12 text-center">
                <h2 class="font-900"><span class="colorefirst fs-1 uppercase">Contattaci</span></h2>
            </div>
            <div class="col-12">
                <div class="custom-form mt-4 pt-4">
                    <div id="message"></div>
                    <form method="post" action="{{route('email')}}" name="contact-form" >
                        @csrf
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="name" class="colorefirst">Nome</label>
                                    <input name="name" id="name" type="text" class="form-control contactbg" placeholder="Il tuo nome..." style="color:white">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="email" class="colorefirst">Email</label>
                                    <input name="email" id="email" type="email" class="form-control contactbg" placeholder="La tua email..." style="color:white">
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="subject" class="colorefirst">Oggetto</label>
                                    <input name="subject" id="subject" type="text" class="form-control contactbg" placeholder="Oggetto..." style="color:white">
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="subject" class="colorefirst">Telefono</label>
                                    <input name="telefono" id="telefono" type="text" class="form-control contactbg" placeholder="numero di telefono..." style="color:white">
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="comments" class="colorefirst">Messaggio</label>
                                    <textarea name="comments" id="comments" rows="7" class="form-control contactbg" placeholder="Scrivi il tuo messagio..." style="color:white"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-lg-12">
                                <button name="send" class="submitBnt btn btn-custom contactbg">Invia Messaggio</button>
                                <div id="simple-msg"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            {{-- <div class="col-lg-5">
                <div class="contact-detail mt-5 pt-4">

                    <p class="mb-2"><i class="mdi mdi-email-outline text-muted"></i> : <span>support@website.com</span></p>
                    <p class="mb-2"><i class="mdi mdi-map-marker text-muted"></i> : <span>97 Ilchester Road, Muirhead, KY15 2GP</span></p>
                    <p class="mb-2"><i class="mdi mdi-cellphone text-muted"></i> : <span>+1 234 569 798</span></p>
                    <p class="mb-0"><i class="mdi mdi-earth text-muted"></i> : <span>www.website.com</span></p>

                    <div class="contact-img pt-4">
                        <img src="images/contact-img.jpg" class="img-fluid" alt="">
                    </div>
                </div>

            </div> --}}

        </div>
    </div>
</section>
<!-- END CONTACT -->

<!-- START FOOTER -->
<footer class="section bg-footer">
    <div class="container reveal">
        <div class="row">

            <div class="col-lg-3">
                <div class="footer-contant">
                    <h6 class="footer-title">Informazioni</h6>
                    <ul class="list-unstyled footer-link mt-4">
                        <li><a href="">Pages</a></li>
                        <li><a href="">Our Team</a></li>
                        <li><a href="">Feuchers</a></li>
                        <li><a href="">Pricing</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="footer-contant">
                    <h6 class="footer-title">Help</h6>
                    <ul class="list-unstyled footer-link mt-4">
                        <li><a href="">Terms of Services</a></li>
                        <li><a href="">Privacy Policy</a></li>
                        <li><a href="{{route('login')}}">Login</a></li>

                    </ul>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="footer-contant">
                    <h6 class="footer-title">Contatti</h6>
                    <p class="contact-info mt-4">Email: <span class="colorefirst">luxury@gmail.com</span></p>
                    <p class="contact-info">Telefono: <span class="colorefirst">0950000000</span></p>
                    <div class="mt-4">
                        <ul class="list-inline social mb-0">
                            <li class="list-inline-item"><a href="" class="social-icon text-muted"><i class="colorefirst mdi mdi-facebook"></i></a></li>
                            <li class="list-inline-item"><a href="" class="social-icon text-muted"><i class="colorefirst mdi mdi-twitter"></i></a></li>
                            <li class="list-inline-item"><a href="" class="social-icon text-muted"><i class="colorefirst mdi mdi-linkedin"></i></a></li>
                            <li class="list-inline-item"><a href="" class="social-icon text-muted"><i class="colorefirst mdi mdi-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>

    </div>


</footer>
<!-- END FOOTER -->

<!-- Back to top -->
{{-- <a href="#home" class="back-to-top" id="back-to-top"> <i class="mdi mdi-chevron-up"> </i> </a> --}}

<script>
function reveal() {
  var reveals = document.querySelectorAll(".reveal");

  for (var i = 0; i < reveals.length; i++) {
    var windowHeight = window.innerHeight;
    var elementTop = reveals[i].getBoundingClientRect().top;
    var elementVisible = 150;

    if (elementTop < windowHeight - elementVisible) {
      reveals[i].classList.add("active");
    } else {
      reveals[i].classList.remove("active");
    }
  }
}

window.addEventListener("scroll", reveal);
</script>
</x-layout>